package twelve

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os/exec"
	"sync"

	log "github.com/sirupsen/logrus"
)

// dlv debug --headless --api-version=2 --log --listen=127.0.0.1:8181

var BASE_ARGS = []string{"--headless"}

type DelveConn struct {
	cmd    *exec.Cmd
	ctx    context.Context
	cancel context.CancelFunc

	addr string

	// b is used to buffer delve output.
	b *blockBuffer
	// writer is an optional sink of delve output.
	writer io.Writer
}

func Launch(out io.Writer, args ...string) *DelveConn {
	ctx, cancel := context.WithCancel(context.Background())

	tmp := make([]string, 0, len(BASE_ARGS)+len(args))
	tmp = append(tmp, BASE_ARGS...)
	tmp = append(tmp, args...)

	c := &DelveConn{
		cmd:    exec.CommandContext(ctx, "dlv", tmp...),
		ctx:    ctx,
		cancel: cancel,
		b:      newBlockBuffer(),
	}

	// Redirect stdout/err
	c.cmd.Stdout = io.MultiWriter(c.b, out) // TODO: to a user provided writer?
	c.cmd.Stderr = io.MultiWriter(c.b, out) // or debugging can just ask for --log

	if err := c.cmd.Start(); err != nil {
		log.Panic("dlv cmd failed", err)
	}

	fmt.Fscanf(c.b, "API server listening at: %s", &c.addr)

	return c
}

func (c *DelveConn) Addr() string {
	return c.addr
}

// Stop asks the server to shut down and closes any connection to it.
// Returns after server is done.
func (c *DelveConn) Stop() {
	c.cancel()
	c.b.Close()
	// TODO: Grab this error
	c.cmd.Wait()
}

// TODO: Need a byte buffer that blocks instead of EOF if the writer
// has not yet produced anything to read.
// Might as well have a context, but that breaks io.Reader?
type blockBuffer struct {
	cond   *sync.Cond
	buf    []byte
	closed bool
}

func newBlockBuffer() *blockBuffer {
	return &blockBuffer{
		cond:   sync.NewCond(&sync.Mutex{}),
		buf:    make([]byte, 0),
		closed: false,
	}
}

func (bb *blockBuffer) Close() {
	bb.cond.L.Lock()
	defer bb.cond.L.Unlock()
	bb.closed = true
}

func (bb *blockBuffer) Write(p []byte) (int, error) {
	if len(p) == 0 {
		return 0, nil
	}

	bb.cond.L.Lock()
	defer bb.cond.L.Unlock()

	if bb.closed {
		return 0, errors.New("closed")
	}

	bb.buf = append(bb.buf, p...)
	bb.cond.Broadcast()
	return len(p), nil
}

func (bb *blockBuffer) Read(buf []byte) (int, error) {
	bb.cond.L.Lock()
	defer bb.cond.L.Unlock()

	for len(bb.buf) == 0 {
		if bb.closed {
			return 0, errors.New("closed")
		}
		bb.cond.Wait()
	}

	n := copy(buf, bb.buf)
	bb.buf = bb.buf[n:]

	return n, nil
}
