package main

import (
	"fmt"
	"log"

	"github.com/jroimartin/gocui"
)

// Types
type action func(*gocui.Gui, *gocui.View) error

// Configuration
const (
	VIEW_ALL     = ""
	VIEW_SRC     = "SRC"
	VIEW_GOPHERS = "GOPHERS"
	VIEW_CMD     = "CMD"
)

var keybinds = map[interface{}]action{
	gocui.KeyCtrlC: quit,
	gocui.KeyTab:   swap,
	'n':            nil,
}

// Runetime variables
var (
	inSource = true
)

func swap(g *gocui.Gui, v *gocui.View) error {
	var name string
	if inSource {
		name = VIEW_GOPHERS
		inSource = false
	} else {
		name = VIEW_SRC
		inSource = true
	}

	if _, err := g.SetCurrentView(name); err != nil {
		return err
	}
	_, err := g.SetViewOnTop(name)
	return err
}

func quit(g *gocui.Gui, v *gocui.View) error {
	return gocui.ErrQuit
}

func layout(g *gocui.Gui) error {
	x, y := g.Size()
	cut := int(0.8 * float64(y))

	if cmd, err := g.SetView(VIEW_CMD, 0, cut, x-1, y); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		fmt.Fprintln(cmd, "> cmd...")
	}

	if gophers, err := g.SetView(VIEW_GOPHERS, -1, -1, x, cut); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		fmt.Fprintln(gophers, "This is gophers window!")
	}

	src, err := g.SetView(VIEW_SRC, -1, -1, x, cut)
	if err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		fmt.Fprintln(src, "This is source window!")
	}

	return nil
}

func main() {
	g, err := gocui.NewGui(gocui.OutputNormal)
	if err != nil {
		log.Panicln(err)
	}
	defer g.Close()

	g.SetManagerFunc(layout)

	for key, action := range keybinds {
		if err := g.SetKeybinding(VIEW_ALL, key, gocui.ModNone, action); err != nil {
			log.Panicf("error setting key %#v to %#v: %v", key, action, err)
		}
	}

	if err := g.MainLoop(); err != gocui.ErrQuit {
		log.Panicln(err)
	}
}
