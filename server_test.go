package twelve

import (
	"bufio"
	"bytes"
	"io"
	"net"
	"os"
	"path/filepath"
	"testing"
	"time"
)

const TEST_PROGRAM = `package main

func main() {

}
`

func TestCreation(t *testing.T) {

	c := Launch(io.Discard)
	defer c.Stop()
	c.Stop()

	if c.ctx.Err() == nil {
		t.Fatal("client context is not cancelled after stop")
	}
}

func TestOutputCapture(t *testing.T) {

	b := bytes.NewBuffer([]byte{})

	c := Launch(b)
	defer c.Stop()
	c.cmd.Wait()

	sc := bufio.NewScanner(b)
	sc.Scan()
	line := sc.Text()
	if line != "Delve is a source level debugger for Go programs." {
		t.Fatalf("expected Delve greeting but got: '%s'", line)
	}

	if err := sc.Err(); err != nil {
		t.Error(err)
	}
}

func TestDebug(t *testing.T) {
	path, err := tmpfile(t, "program.go", TEST_PROGRAM)
	if err != nil {
		t.Error(err)
	}

	c := Launch(io.Discard, "debug", path)
	defer c.Stop()

	_, err = net.DialTimeout("tcp", c.addr, time.Second)
	if err != nil {
		t.Fatal(err)
	}
}

func tmpfile(t *testing.T, name, content string) (string, error) {
	path := filepath.Join(t.TempDir(), name)

	f, err := os.Create(path)
	if err != nil {
		return "", err
	}
	defer f.Close()

	_, err = f.WriteString(content)
	return path, err
}
