module gitlab.com/vikblom/twelve

go 1.17

require (
	github.com/go-delve/delve v1.8.0
	github.com/jroimartin/gocui v0.5.0
	github.com/sirupsen/logrus v1.8.1
)

require (
	github.com/cilium/ebpf v0.7.0 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/nsf/termbox-go v1.1.1 // indirect
	golang.org/x/arch v0.0.0-20190927153633-4e8777c89be4 // indirect
	golang.org/x/sys v0.0.0-20211019181941-9d821ace8654 // indirect
)
