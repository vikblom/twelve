package twelve

import (
	"net/rpc/jsonrpc"

	"github.com/go-delve/delve/service/api"
)

func Dial(addr string) error {
	client, err := jsonrpc.Dial("tcp", addr)
	if err != nil {
		return err
	}
	defer client.Close()

	in := api.SetAPIVersionIn{
		APIVersion: 2,
	}

	return client.Call("RPCServer.SetApiVersion", in, &api.SetAPIVersionOut{})
}
