# twelve

TUI client for Delve.

## Go terminal packages

https://appliedgo.net/tui/

Low:
- https://github.com/nsf/termbox-go
- https://github.com/gdamore/tcell
High:
- https://github.com/jroimartin/gocui
