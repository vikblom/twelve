package twelve_test

import (
	"flag"
	"fmt"
	"io"
	"net"
	"os"
	"testing"

	"gitlab.com/vikblom/twelve"
)

var sink io.Writer = io.Discard

func TestMain(m *testing.M) {
	flag.Parse()
	if testing.Verbose() {
		sink = os.Stdout
	}
	os.Exit(m.Run())
}

func addrToHex4(addr *net.TCPAddr) string {
	// For details about the format, see the kernel side implementation:
	// https://elixir.bootlin.com/linux/v5.2.2/source/net/ipv4/tcp_ipv4.c#L2375
	b := addr.IP.To4()
	return fmt.Sprintf("%02X%02X%02X%02X:%04X", b[3], b[2], b[1], b[0], addr.Port)
}

func TestClientConnect(t *testing.T) {

	d := twelve.Launch(sink, "debug", "./cmd/tui.go") // FIXME

	t.Log(d.Addr())
	addr, err := net.ResolveTCPAddr("tcp", d.Addr())
	if err != nil {
		t.Fatal(err)
	}
	//t.Log("HEX:", addrToHex4(addr))

	err = twelve.Dial(addr.String())
	if err != nil {
		t.Fatal(err)
	}
}
